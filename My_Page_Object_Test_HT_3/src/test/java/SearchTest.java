import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTest extends BaseTest {

    private String SEARCH_KEYWORD = "Условия рассрочки";
    private String EXPECTED_SEARCH_QUERY = "usloviya-rassrochki";
    private String SEARCH_KEYWORD_ANOTHER = "Samsung";

    @Test(priority = 1)
    public void checkElementsByLinks() {
        getHomePage().searchElementByKeyword(SEARCH_KEYWORD);
        assertTrue(getDriver().getCurrentUrl().contains(EXPECTED_SEARCH_QUERY));
    }

    @Test(priority = 2)
    public void checkAmountOfElement() {
        getHomePage().clickOnSamsungButton();
        getBrandSamsung().implicitWait(30);
        getBrandSamsung().choiseTvsets();
        assertEquals(getBrandSamsung().getSearchResultsCount(), 4);
    }

    @Test(priority = 3)
    public void checkSearchingElements() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD_ANOTHER);
        for (WebElement webElement : getSearchResult().getSearchResultsList()) {
            assertTrue(webElement.getText().contains(SEARCH_KEYWORD_ANOTHER));
        }

    }





}
