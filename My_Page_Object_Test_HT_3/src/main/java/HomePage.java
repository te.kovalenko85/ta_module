import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

    private By placeholder = By.xpath("//input[contains(@placeholder,'Поиск')]");
    private By elementByLinks = By.linkText("Условия рассрочки");
    private By brandSamsung = By. xpath ("//div[@class='partner-box height']/a[@href='/brand-samsung']"); //бренд Самсунг
    private By AMOUNT_OF_GOODS_IN_CART = By.xpath("//div[contains(@class,'header-bottom__cart')]//div[contains(@class,'cart_count')]");

    public HomePage (WebDriver driver) {
        super(driver);
    }

    public void searchElementByKeyword(final String keyword) {
        driver.findElement(elementByLinks).sendKeys(keyword, Keys.ENTER); }

    public void clickOnSamsungButton () {
            driver.findElement(brandSamsung).click();
        }

    public void searchByKeyword(final String keyword) {
        driver.findElement(placeholder).sendKeys(keyword, Keys.ENTER);
    }

    public String checkAmountOfGoodsInCart () {
        return driver.findElement(AMOUNT_OF_GOODS_IN_CART).getText();
    }


}
