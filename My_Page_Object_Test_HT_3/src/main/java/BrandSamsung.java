import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Collection;
import java.util.List;

public class BrandSamsung extends BasePage {

    private By tvSetsItem = By.xpath("//li//a[text()='Телевизоры' and @class='category-box-item']"); //выбрали из бокс-айтем телевизоры
    private By AMOUNT_OF_SAMSUNG_GOODS_ON_PAGE = By.xpath("//div[@class='prod-cart__descr'] [contains(text(),'Телевизор Samsung')]"); //собрали элементы поиска в лист

    public BrandSamsung(WebDriver driver) {
        super(driver);
    }

    public void choiseTvsets () {
        driver.findElement(tvSetsItem).click();
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size(); //размер листа (кол-во элементов)
    }
    public List<WebElement> getSearchResultsList() {
        return driver.findElements(AMOUNT_OF_SAMSUNG_GOODS_ON_PAGE); //возвращает кол-во элементов
    }

}


