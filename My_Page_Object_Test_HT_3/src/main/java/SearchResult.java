import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.awt.*;
import java.util.Collection;
import java.util.List;

public class SearchResult extends BasePage {


    private By nameOfGoodsIsSamsung =By.xpath("//div[@class='prod-cart__descr'][contains(text(),'Samsung')]");
    private By categoryTv = By.xpath("//span[@data-counter='categories_982']");//чек-бокс ТВ

    private By tvSony = By.xpath("//a[@class='prod-cart__buy'][contains(@data-ecomm-cart, 'Samsung 55Q70R')]"); //выбрали ТВ,кнопка КУпить
    private By ADD_TO_CART_POPUP = By.id("js_cart");
    private By continueShoppingButton = By.xpath("//a[contains(@class, 'btn--orange')][contains(text(),'Продолжить покупки')]");


    public SearchResult(WebDriver driver) {
        super(driver);
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }
    public List<WebElement> getSearchResultsList() {
        return driver.findElements((By) nameOfGoodsIsSamsung);//проверили что каждый элемент листа содержит текст Samsung

    }

    public void buyButton () {
        driver.findElement(tvSony).click();
    }

        public By getAddToCartPopup() {
            return ADD_TO_CART_POPUP;
        }

    public void clickContinueShoppingButton () {
        driver.findElement(continueShoppingButton).click();
    }




}







