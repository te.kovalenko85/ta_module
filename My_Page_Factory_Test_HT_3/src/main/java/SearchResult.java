import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.util.Collection;
import java.util.List;

public class SearchResult extends BasePage {

    @FindBy(xpath = "//div[@class='prod-cart__descr'][contains(text(),'Samsung')]")
    private List<WebElement> nameOfGoodsIsSamsung;

    @FindBy(xpath = "//span[@data-counter='categories_982']")
    private WebElement categoryTv;

    @FindBy(xpath = "//a[@class='prod-cart__buy'][contains(@data-ecomm-cart, 'Samsung 55Q70R')]")
    private WebElement tvSony;

    @FindBy(id="js_cart")
    private WebElement ADD_TO_CART_POPUP;

    @FindBy(xpath = "//a[contains(@class, 'btn--orange')][contains(text(),'Продолжить покупки')]")
    private WebElement continueShoppingButton;

    public SearchResult(WebDriver driver) { super(driver); }

    public int getSearchResultsCount() { return getSearchResultsList().size();
    }
    public List<WebElement> getSearchResultsList() { return nameOfGoodsIsSamsung; }

    public void buyButton () { tvSony.click();}

    public WebElement getAddToCartPopup() { return ADD_TO_CART_POPUP; }

    public void clickContinueShoppingButton () { continueShoppingButton.click(); }

}







