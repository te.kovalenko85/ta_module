import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[contains(@placeholder,'Поиск')]")
    private WebElement placeholder;

    @FindBy(xpath = "//a[@href='/usloviya-rassrochki' and @class='header-top__item']")
    private WebElement elementByLinks;

    @FindBy(xpath = "//div[@class='partner-box height']/a[@href='/brand-samsung']")
    private WebElement brandSamsung;

    @FindBy(xpath = "//div[contains(@class,'header-bottom__cart')]//div[contains(@class,'cart_count')]")
    private WebElement  AMOUNT_OF_GOODS_IN_CART;

    public HomePage (WebDriver driver) { super(driver); }

        public void searchElementByKeyword(final String keyword) { elementByLinks.sendKeys(keyword, Keys.ENTER); }

        public void clickOnSamsungButton () { brandSamsung.click();}

        public void searchByKeyword(final String keyword) { placeholder.sendKeys(keyword, Keys.ENTER); }

        public String checkAmountOfGoodsInCart () {
        return AMOUNT_OF_GOODS_IN_CART.getText();
    }

}
