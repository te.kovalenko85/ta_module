import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Collection;
import java.util.List;

public class BrandSamsung extends BasePage {

    @FindBy(xpath = "//li//a[text()='Телевизоры' and @class='category-box-item']")
    private WebElement tvSetsItem;

    @FindBy(xpath = "//div[@class='prod-cart__descr'] [contains(text(),'Телевизор Samsung')]")
    private List<WebElement> AMOUNT_OF_SAMSUNG_GOODS_ON_PAGE;

    public BrandSamsung(WebDriver driver) { super(driver); }

    public void choiseTvsets () { tvSetsItem.click();
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }

    public List<WebElement> getSearchResultsList() { return AMOUNT_OF_SAMSUNG_GOODS_ON_PAGE; }

}


