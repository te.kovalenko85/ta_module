import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AddToKartTest extends BaseTest {


    private String SEARCH_KEYWORD_ANOTHER = "Samsung Телевизоры";
    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART = "1";

    @Test (priority = 4)
    public void checkAddToCart() throws InterruptedException {
        getHomePage().clickOnSamsungButton();
        getBrandSamsung().implicitWait(30);
        getBrandSamsung().choiseTvsets();
        getHomePage().searchByKeyword(SEARCH_KEYWORD_ANOTHER);
        getSearchResult().buyButton();
        getSearchResult().waitVisibilityOfElement(30, getSearchResult().getAddToCartPopup());
        getSearchResult().clickContinueShoppingButton();
        Thread.sleep(4000);
        assertEquals(getHomePage().checkAmountOfGoodsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
    }
}
