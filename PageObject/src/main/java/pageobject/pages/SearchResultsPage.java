package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResultsPage extends BasePage {

    private static final By SEARCH_RESULTS_PRODUCTS_LIST_TEXT = By.xpath("//div[@class='prod-cart__descr']");

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }

    public List<WebElement> getSearchResultsList() {
        return driver.findElements(SEARCH_RESULTS_PRODUCTS_LIST_TEXT);
    }
}
