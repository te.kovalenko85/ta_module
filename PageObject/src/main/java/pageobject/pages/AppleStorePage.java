package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AppleStorePage extends BasePage {

    private static final By IPHONE_BUTTON = By.xpath("//div[@class='brand-box__title']/a[contains(@href,'iphone')]");

    public AppleStorePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnIphoneButton() {
        driver.findElement(IPHONE_BUTTON).click();
    }

}
