package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IphonePage extends BasePage {

    private static final By ADD_TO_CART_BUTTON = By.xpath("//a[@class='prod-cart__buy'][contains(@data-ecomm-cart,' Pacific Blue (MGDL3)')]");
    private static final By ADD_TO_CART_POPUP = By.id("js_cart");
    private static final By CONTINUE_SHOPPING_BUTTON = By.xpath("//div[@class='btns-cart-holder']//a[contains(@class,'btn--orange')]");

    public IphonePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnAddToCartButton() {
        driver.findElement(ADD_TO_CART_BUTTON).click();
    }

    public void clickOnContinueShoppingButton() {
        driver.findElement(CONTINUE_SHOPPING_BUTTON).click();
    }

    public By getAddToCartPopup() {
        return ADD_TO_CART_POPUP;
    }
}
