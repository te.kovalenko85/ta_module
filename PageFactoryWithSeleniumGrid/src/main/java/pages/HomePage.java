package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {

    @FindBy(xpath = "//div[contains(@class, 'main-header main-wrapper')]")
        private WebElement header;

    @FindBy(xpath = "//footer")
        private WebElement footer;

    @FindBy(xpath ="//form[@id='main-header_search']")
            private WebElement searchField;

    @FindBy(xpath = "//a[@href='/uk/homepage']")
            private WebElement clickLanguageButtonUa;

    @FindBy(xpath = "//li[@class='main-nav_list-element main-nav_list-element_minicart' and @data-js-cart-wrapper]")
        private WebElement cartIcon;

    @FindBy(xpath = "//a[@href='/login']")
        private WebElement myAccount;

    @FindBy(xpath = "//input[@ type='search']")
        private WebElement inputSearchField;

    @FindBy(xpath = "//a[@href='/cart']")
        private WebElement cartButton;



    public HomePage(WebDriver driver) { super(driver); }


public void visibilityOfHeader() { header.isDisplayed(); }

public void visibilityOfFooter() { footer.isDisplayed(); }

public void visibilityOfSearchField() { searchField.isDisplayed(); }

public String checkLanguage() { return clickLanguageButtonUa.getText(); }

public void visibilityOfCartIcon() { cartIcon.isDisplayed(); }

public void visibilityOfMyAccount() { myAccount.isDisplayed(); }

public void clickMyAccount() { myAccount.click();}

public void findKeyWord(final String keyword) { inputSearchField.sendKeys(keyword, Keys.ENTER);}

public void clickCartButton() { cartButton.click();}

}
