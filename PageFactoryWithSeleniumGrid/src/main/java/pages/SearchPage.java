package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage extends BasePage {

    @FindBy(xpath = "//button[@data-js-form-target='addToCartForm-70004' and @aria-haspopup='dialog']")
        private WebElement addToCartButton;

    @FindBy(xpath = "//div[@id='popup_add-to-cart']//a[@href='/cart']")
        private WebElement goToCart;


    public SearchPage(WebDriver driver) {super(driver);}

    public void clickAddToCart () { addToCartButton.click(); }

    public void clickGoToCart() {goToCart.click();}
}
