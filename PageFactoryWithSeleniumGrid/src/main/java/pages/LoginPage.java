package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//a[@href='/login']")
        private WebElement loginOption;

    @FindBy(xpath = "//div[@id='js-modal-page']")
        private WebElement modalPage;

    @FindBy(xpath = "//input[@type='email']")
        private WebElement emailField;

    @FindBy(xpath = "//input[@type='password']")
        private WebElement passwordField;

    @FindBy(xpath = "//a[@href='/pre-register']")
        private WebElement registerOption;

    @FindBy(xpath = "//input[@type='email']")
        private WebElement emailFieldForRegister;


    public LoginPage(WebDriver driver){super(driver);}


    public void clickLoginOption() {loginOption.click();}

    public WebElement getModalPage() { return (WebElement) modalPage; }

    public boolean visibilityOfEmailField(){return emailField.isDisplayed();}

    public boolean visibilityOfPasswordField(){return passwordField.isDisplayed();}

    public void clickRegisterOption(){registerOption.click();}

    public boolean visibilityOfEmailFieldForRegister(){ return emailFieldForRegister.isDisplayed();}
}
