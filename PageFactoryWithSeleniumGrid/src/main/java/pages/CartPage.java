package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage {

    @FindBy(xpath = "//main[@class='row m-b_default']")
    private WebElement orderForm;

    @FindBy(xpath = "//input[@name='productCode']")
    private WebElement productCodeField;

    @FindBy(xpath = "//select[@name='quantity']/option[@value=\"2\"]")
    private WebElement addingOneMoreGood;

    @FindBy(xpath = "//select[contains(@class,'quantity')]")
    private WebElement addButton;

    @FindBy(xpath = "//a[@href='/cart'][contains(text(), '2')]")
    private WebElement quantityGoodsAfterAdding;

    @FindBy(xpath = "//form[@id='goto-shipping-form']")
    private WebElement shoppingForm;

    @FindBy(xpath = "//a[@href='/cart']")
    private WebElement AMOUNT_OF_GOODS_IN_CART;

    public CartPage(WebDriver driver) {
        super(driver);
    }


    public WebElement getOrderForm() {
        return (WebElement) orderForm;
    }

    public void searchProductByCode(final String keyword) {
        productCodeField.sendKeys(keyword, Keys.ENTER);
    }

    public void addOneMoreGood() {
        addingOneMoreGood.click();
    }

    public void clickAddButton() {
        addButton.click();
    }

    public String totalGoodsAfterAdding() {
        return quantityGoodsAfterAdding.getText();
    }

    public boolean visibilityOfOrder() { return shoppingForm.isDisplayed();}

    public String amountOfGoodsInCart () { return AMOUNT_OF_GOODS_IN_CART.getText(); }

}
