package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;

import pages.CartPage;
import pages.HomePage;
import pages.LoginPage;
import pages.SearchPage;

import utils.CapabilityFactory;

public class BaseTest {

    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
    private CapabilityFactory capabilityFactory = new CapabilityFactory();

    private static final String YVES_ROCHER_URL = "https://www.yves-rocher.ua/";

    @BeforeMethod
    @Parameters(value = {"browser"})
    public void setUp(String browser) throws MalformedURLException {
        driver.set(new RemoteWebDriver(new URL("http://192.168.1.182:4445/wd/hub"), capabilityFactory.getCapabilities(browser)));
        getDriver().get(YVES_ROCHER_URL);
    }

    @AfterMethod
    public void tearDown() {
        getDriver().close();
    }

    @AfterClass
    void terminate() {

        driver.remove();
    }

    public WebDriver getDriver() {
        return driver.get();
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public CartPage getCartPage() {
        return new CartPage(getDriver());
    }

    public LoginPage getLoginPage() { return new LoginPage(getDriver()); }

    public SearchPage getSearchPage() { return new SearchPage(getDriver()); }

}
