package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SmokeTests extends BaseTest {

    private static final long DEFAULT_WAITING_TIME = 60;
    String language = "uk";
    String keyword = "волосся";
    String keywordCode = "A67390";
    String EXPECTED_OF_AMOUNT = "2";

    @Test
    public void checkElementsOnHomePage() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        //getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().visibilityOfHeader();
        getHomePage().visibilityOfFooter();
        getHomePage().visibilityOfSearchField();
        //assertTrue(getHomePage().checkLanguage().contains(language));
        getHomePage().visibilityOfCartIcon();
        getHomePage().visibilityOfMyAccount();
        getHomePage().clickMyAccount();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        //getLoginPage().clickLoginOption();
        //getLoginPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getLoginPage().getModalPage());
        assertTrue(getLoginPage().visibilityOfEmailField());
        assertTrue(getLoginPage().visibilityOfPasswordField());
        getLoginPage().clickRegisterOption();
        //getLoginPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getLoginPage().getModalPage());
        assertTrue(getLoginPage().visibilityOfEmailFieldForRegister());
    }

    @Test
    public void checkFindAndAddElementToCart() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        //getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().findKeyWord(keyword);
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getSearchPage().clickAddToCart();
        getSearchPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        //getSearchPage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getSearchPage().clickGoToCart();
        assertEquals(getCartPage().amountOfGoodsInCart(), "1");
    }

    @Test
    public void checkPaymentForm() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        //getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().clickCartButton();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getCartPage().getOrderForm();
        getCartPage().searchProductByCode(keywordCode);
        getCartPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        //getCartPage().addOneMoreGood();
        //getCartPage().clickAddButton();
        //assertEquals(getCartPage().totalGoodsAfterAdding(),EXPECTED_OF_AMOUNT);
        assertTrue(getCartPage().visibilityOfOrder());
    }
}
