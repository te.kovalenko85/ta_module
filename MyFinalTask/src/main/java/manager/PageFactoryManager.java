package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public ViewingPage getViewingPage() {
        return new ViewingPage(driver);
    }

    public SelectPage getSelectPage() {
        return new SelectPage(driver);
    }

    public RegisterPage getRegisterPage() {
        return new RegisterPage(driver);
    }

    public IdentityRegisterPage getIdentityRegisterPage() {
        return new IdentityRegisterPage(driver);
    }

    public HourglassjeansPage getHourglassjeansPage() {
        return new HourglassjeansPage(driver);
    }

    public SavedListPage savedListPage() {
        return new SavedListPage(driver);
    }
}
