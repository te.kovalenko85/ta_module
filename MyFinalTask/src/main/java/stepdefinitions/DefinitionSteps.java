package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {
    private static final long DEFAULT_TIMEOUT = 60;
    WebDriver driver;
    HomePage homePage;
    RegisterPage registerPage;
    SelectPage selectPage;
    ViewingPage viewingPage;
    IdentityRegisterPage identityRegisterPage;
    HourglassjeansPage hourglassjeansPage;
    SavedListPage savedListPage;
    PageFactoryManager pageFactoryManager;


    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User sees logo")
    public void userSeesLogo() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.visibilityOfLogo());
    }

    @When("User clicks women button")
    public void userClicksWomenButton() {
        homePage.clickWomenButton();
    }

    @Then("User can see the page of women's products")
    public void checkCurrentUrlForWomenProductPage() {
        assertTrue(driver.getCurrentUrl().contains("women"));
    }

    @When("User clicks men button")
    public void userClicksMenButton() {
        homePage.clickMenButton();
    }

    @Then("User can see the page of men's products")
    public void checkCurrentUrlForMenProductPage() {
        assertTrue(driver.getCurrentUrl().contains("men"));
    }

    @And("User checks the searchField")
    public void userChecksTheSearchField() {
        homePage.visibilityOfSearchField();
    }

    @And("User clicks account button")
    public void userClicksAccountButton() {
        homePage.clickMyAccountDropdown();
    }

    @And("User can see popup to SignIn or Join")
    public void userCanSeePopupToSignInOrJoin() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSignInPopupElement());
        assertTrue(homePage.getSignInPopup());
    }

    @And("User clicks savedItemsButton")
    public void userClicksSavedItemsButton() {
        homePage.clickSavedItemsButton();
        selectPage = pageFactoryManager.getSelectPage();
        selectPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        selectPage.visibilitySavedLists();
        assertTrue(selectPage.visibilitySignInButtonInSavedList());
    }

    @And("User checks the bagButton")
    public void userChecksTheBagButton() {
        homePage.visibilityBagButton();
    }

    @When("User clicks bag button")
    public void userClicksBagButton() {
        homePage.clickBagButton();
    }

    @Then("User can see bag page and SignIn button")
    public void userCanSeeBagPageAndSignInButton() {
        selectPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        selectPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, selectPage.buttonSigiIn());
        selectPage.visibilitySignInButton();
    }

    @And("User can see footer")
    public void userCanSeeFooter() {
        homePage.visibilityFooter();
    }

    @And("User clicks country selector button")
    public void userClicksCountrySelectorButton() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.clickCountrySelectorButton();
    }

    @And("User can see popup of preferences")
    public void userCanSeePopupOfPreferences() {
        assertTrue(homePage.getSelectCountryPopup());
    }

    @And("User choose country")
    public void userChooseCountry() {
        homePage.clickIdCountry();
        homePage.selectCountryName();
        homePage.clickSubmitButton();
    }

    @And("User checks that country selector {string}")
    public void checkIsCountrySelector(final String country) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(driver.getCurrentUrl(), country);
    }

    @And("User clicks MyAccount button")
    public void userClicksMyAccountButton() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.clickMyAccountDropdown();
        homePage.getSignInPopup();
    }

    @And("User clicks Join tab")
    public void userClicksJoinTab() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSignInPopupElement());
        homePage.clickJoinTab();
        registerPage = pageFactoryManager.getRegisterPage();
        assertTrue(registerPage.visibilityRegisterForm());
    }

    @And("User fills the register form with {string}, {string}, {string}, {string}")
    public void userFillsTheRegisterFormWithEmailAddressFirstNameLastNamePassword(final String email, final String firstname,
                                                                                  final String lastname, final String password) {
        registerPage.enterEmailAndFirstNameAndLastNameAndPassword(email, firstname,
                lastname, password);
        registerPage.clickBirthDayBox();
        registerPage.chooseDayOfBirthday();
        registerPage.clickMonthDayBox();
        registerPage.chooseMonthBirthDay();
        registerPage.clickYearBirthDay();
        registerPage.chooseYearBirthDay();
    }

    @When("User clicks Join")
    public void userClicksJoin() {
        registerPage.clickJoinAsosTab();
    }

    @Then("User can see the offer to finish registration")
    public void userCanSeeTheOfferToFinishRegistration() {
        identityRegisterPage = pageFactoryManager.getIdentityRegisterPage();
        registerPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(identityRegisterPage.visibilityRegisterOtherForm());
    }

    @When("User fills the register form with {string}, {string}, {string}")
    public void userFillsTheRegisterFormWithEmailAddressLastNamePassword(final String email, final String lastname, final String password) {
        registerPage.enterEmailAndLastNameAndPassword(email, lastname, password);
        registerPage.clickJoinAsosTab();
    }

    @Then("User can see a warning message")
    public void userCanSeeAWarningMessage() {
        registerPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, registerPage.getFirstNameError());
        assertTrue(registerPage.visibilityFirstNameError());
    }

    @And("User fills the register form in fields {string}, {string}, {string}, {string}")
    public void userFillsTheRegisterForm(final String email, final String firstname, final String lastname, final String password) {
        registerPage.enterEmailAndFirstNameAndLastNameAndPassword(email, firstname,
                lastname, password);
    }

    @When("User chooses nonexistent day of month")
    public void chooseNonexistentDayOfMonth() {
        registerPage.clickBirthDayBox();
        registerPage.chooseNonexistentDayOfMonth();
        registerPage.clickMonthDayBox();
        registerPage.chooseMonth();
    }

    @Then("User sees message about wrong date {string}")
    public void userSeesMessageAboutWrongDateWrongMessage(final String expectedText) {
        registerPage.getMonthOfBirthdayError().isDisplayed();
        assertEquals(registerPage.getMonthErrorText(), expectedText);
    }

    @And("User enters {string} to find the good")
    public void userEntersKeywordToFindTheGood(final String keyword) {
        homePage.enterNameOfGoodInSearchField(keyword);
    }

    @And("User can see the list of goods by keyword")
    public void userCanSeeTheListOfGoodsByKeyword() {
        assertTrue(homePage.checkThatSearchResultsContainsKeyword());
    }

    @And("User clicks shop women button")
    public void userClicksShopWomenButton() {
        homePage.clickShopWomenButton();
    }

    @And("User chooses category of goods HOURGLASS JEANS")
    public void userChoosesCategoryOfGoods() {
        viewingPage = pageFactoryManager.getViewingPage();
        viewingPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        viewingPage.chooseCategoryOfGoods();
    }

    @Then("User chooses the items")
    public void userChoosesTheItems() {
        hourglassjeansPage = pageFactoryManager.getHourglassjeansPage();
        hourglassjeansPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        hourglassjeansPage.clickIdItem();
        hourglassjeansPage.clickAnotherIdItem();
    }

    @And("User clicks saved items button")
    public void userClicksSavedItButton() {
        hourglassjeansPage.clickOnSavedItemsButton();
    }

    @And("User can see chosen goods")
    public void userCanSeeChosenGoods() {
        savedListPage = pageFactoryManager.savedListPage();
        savedListPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        savedListPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, savedListPage.getChosenItems());
        assertTrue(savedListPage.visibilityChosenItems());
    }

    @And("User checks correct number of items {string}")
    public void userChecksCorrectNumberOfItemsQuantity(final String expectedNumber) {
        assertEquals(savedListPage.numberOfItems(), expectedNumber);
    }

    @And("User clicks on the products")
    public void clickOnTheProducts() {
        savedListPage.clickProductSelect();
        savedListPage.clickSelectSizeField();
        savedListPage.clickSelectSize();
    }

    @Then("User clicks the button to add goods")
    public void buttonToAddGoods() {
        savedListPage.clickAddToBAgButton();
    }

    @And("User checks quantity {string} of goods in the cart")
    public void userChecksQuantityNumberOfGoodsInTheCart(final String expectedNumber) {
        savedListPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, savedListPage.getCartPopup());
        assertEquals(savedListPage.getCartButtonText(), expectedNumber);
    }

    @And("User doesn't click on the select size field")
    public void userDoesnTClickOnTheSelectSizeField() {
        savedListPage.clickProductSelect();
    }

    @And("User can see the message {string}")
    public void userCanSeeTheMessageWarningMessage(final String expextedTextError) {
        assertTrue(savedListPage.visibilityTextError());
        assertEquals(savedListPage.getTextError(), expextedTextError);
    }

    @And("User clicks birthday box")
    public void clickBirthdayBox() {
        registerPage.clickBirthDayBox();
        assertTrue(registerPage.getListOfNumberDays());
    }

    @And("User can see days are sorted in ascending")
    public void userCanSeeDaysAreSortedInAscending() {
        assertTrue(registerPage.checkListIsSorted());
    }


    @After
    public void tearDown() {
        driver.close();
    }



}
