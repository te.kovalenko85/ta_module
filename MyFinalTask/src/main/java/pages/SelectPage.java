package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SelectPage extends BasePage {

    @FindBy(xpath = "//main[@id='chrome-app-container']")
    private WebElement savedList;

    @FindBy(xpath = "//button[starts-with(@class, 'button_31vS9')]")
    private WebElement signInButtonInSavedList;

    @FindBy(xpath = "//p[@class='button-holder']")
    private WebElement signInButton;


    public SelectPage(WebDriver driver) {
        super(driver);
    }

    public void visibilitySavedLists() {
        savedList.isDisplayed();
    }

    public boolean visibilitySignInButtonInSavedList() {
        return signInButtonInSavedList.isDisplayed();
    }

    public WebElement buttonSigiIn() {
        return signInButton;
    }

    public void visibilitySignInButton() {
        signInButton.isDisplayed();
    }


}
