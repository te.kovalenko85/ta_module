package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RegisterPage extends BasePage {

    @FindBy(xpath = "//div[@id='register-form']")
    private WebElement registerForm;

    @FindBy(xpath = "//input[@alt='Email']")
    private WebElement emailAddress;

    @FindBy(xpath = "//input[@class='qa-firstname-textbox']")
    private WebElement firstName;

    @FindBy(xpath = "//input[@class='qa-lastname-textbox']")
    private WebElement lastName;

    @FindBy(xpath = "//input[@aria-describedby='PasswordHint']")
    private WebElement passwordField;

    @FindBy(xpath = "//select[@aria-label='Date of birth Day']")
    private WebElement birthDayTextBox;

    @FindBy(xpath = "//select[@name='BirthDay']//option[@value='7']")
    private WebElement dateBirthDay;

    @FindBy(xpath = "//select[@aria-label='Date of birth Month']")
    private WebElement monthBirthDayTextBox;

    @FindBy(xpath = "//option[contains(text(),'July')]")
    private WebElement monthBirthDay;

    @FindBy(xpath = "//select[@ aria-describedby='BirthdayHint']")
    private WebElement yearBirthDayTextBox;

    @FindBy(xpath = "//option[@value='1997']")
    private WebElement yearBirthday;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement joinAsos;

    @FindBy(xpath = "//span[@class='qa-firstname-validation field-validation-error']//span[@id='FirstName-error']")
    private WebElement firstNameError;

    @FindBy(xpath = "//select[@aria-label='Date of birth Day']/option[@value='31']")
    private WebElement nonexistentDate;

    @FindBy(xpath = "//select[@aria-label='Date of birth Month']/option[contains(text(),'June')]")
    private WebElement nameOfMonth;

    @FindBy(xpath = "//span[@id='BirthDay-error']")
    private WebElement monthError;

    @FindBy(xpath = "//select[@name='BirthDay']/option")
    private List<WebElement> listNumbersOfDay;

    @FindBy(xpath = "//select[@name='BirthDay']/option")
    private WebElement listOfElementsNumbersOfDay;

    @FindBy(xpath = "//select[@name='BirthDay']/option[@selected='selected']")
    private WebElement listOfDays;


    public RegisterPage(WebDriver driver) {
        super(driver);
    }


    public boolean visibilityRegisterForm() {
        return registerForm.isDisplayed();
    }

    public void enterEmailAndFirstNameAndLastNameAndPassword(final String email, final String firstname,
                                                             final String lastname, final String password) {
        emailAddress.sendKeys(email);
        firstName.sendKeys(firstname);
        lastName.sendKeys(lastname);
        passwordField.sendKeys(password);
    }

    public void clickBirthDayBox() {
        birthDayTextBox.click();
    }

    public void chooseDayOfBirthday() {
        dateBirthDay.isSelected();
    }

    public void clickMonthDayBox() {
        monthBirthDayTextBox.click();
    }

    public void chooseMonthBirthDay() {
        monthBirthDay.isSelected();
    }

    public void clickYearBirthDay() {
        yearBirthDayTextBox.click();
    }

    public void chooseYearBirthDay() {
        yearBirthday.isSelected();
    }

    public void clickJoinAsosTab() {
        joinAsos.submit();
    }

    public void enterEmailAndLastNameAndPassword(final String email, final String lastname, final String password) {
        emailAddress.sendKeys(email);
        lastName.sendKeys(lastname);
        passwordField.sendKeys(password);
    }

    public WebElement getFirstNameError() {
        return firstNameError;
    }

    public boolean visibilityFirstNameError() {
        return firstNameError.isDisplayed();
    }

    public String getFirstNameErrorText() {
        return firstNameError.getText();
    }

    public void chooseNonexistentDayOfMonth() {
        nonexistentDate.isSelected();
    }

    public void chooseMonth() {
        nameOfMonth.isSelected();
    }

    public WebElement getMonthOfBirthdayError() {
        return monthError;
    }

    public String getMonthErrorText() {
        return monthError.getText();
    }

    public boolean getListOfNumberDays() {
        return listOfDays.isDisplayed();
    }

    public boolean checkListIsSorted() {
        for (int i = 1; i < listNumbersOfDay.size(); i++) {
            if ((i + 1) < listNumbersOfDay.size()) {
                if (Integer.parseInt(listNumbersOfDay.get(i).getText()) > Integer.parseInt(listNumbersOfDay.get(i + 1).getText())) {
                    return false;
                }
            }
        }
        return true;

    }

}