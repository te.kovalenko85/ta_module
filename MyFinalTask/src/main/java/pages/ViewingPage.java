package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ViewingPage extends BasePage {

    @FindBy(xpath = "//a[@class='feature__link'] //p[text()='HOURGLASS JEANS']")
    private WebElement categoryOfGoods;

    public ViewingPage(WebDriver driver) {
        super(driver);
    }

    public void chooseCategoryOfGoods() {
        categoryOfGoods.click();
    }

}
