package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HourglassjeansPage extends BasePage {

    @FindBy(xpath = "//article[@id='product-22030990']//button")
    private WebElement idItem;

    @FindBy(xpath = "//article[@id='product-20895687']//button")
    private WebElement anotherIdItem;

    @FindBy(xpath = "//span[@type='heartUnfilled']")
    private WebElement savedItemsButton;


    public HourglassjeansPage(WebDriver driver) {
        super(driver);
    }


    public void clickIdItem() {
        idItem.click();
    }

    public void clickAnotherIdItem() {
        anotherIdItem.click();
    }

    public void clickOnSavedItemsButton() {
        savedItemsButton.click();
    }
}
