package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class HomePage extends BasePage {

    @FindBy(xpath = "//a[@data-testid='asoslogo']")
    private WebElement logo;

    @FindBy(xpath = "//ul//a[contains(text(),'WOMEN')]")
    private WebElement womenButton;

    @FindBy(xpath = "//a[@id='men-floor']")
    private WebElement menButton;

    @FindBy(xpath = "//input[starts-with(@placeholder,'Search for items')]")
    private WebElement searchField;

    @FindBy(xpath = "//button[@type='button' and @icon='_3iH_8F6']")
    private WebElement myAccountDropdown;

    @FindBy(xpath = "//div[@class='_3hSCfS2']")
    private WebElement signInPopup;

    @FindBy(xpath = "//span[@type='heartUnfilled']")
    private WebElement savedItemsButton;

    @FindBy(xpath = "//span[@type='bagUnfilled']")
    private WebElement bagButton;

    @FindBy(xpath = "//div[@class='_25L--Pi']")
    private WebElement countrySelectorButton;

    @FindBy(xpath = "//div[@class='_3bZNClC']")
    private WebElement countrySelectorForm;

    @FindBy(xpath = "//div[@id='chrome-footer']")
    private WebElement footer;

    @FindBy(xpath = "//select[@id='country']")
    private WebElement idCountry;

    @FindBy(xpath = "//option[@value='AT']")
    private WebElement countryName;

    @FindBy(xpath = "//button[@data-testid='save-country-button']")
    private WebElement submitButton;

    @FindBy(xpath = "//meta[contains(@content, '/de/')]")
    private WebElement urlContent;

    @FindBy(xpath = "//span//a[@data-testid='signup-link']")
    private WebElement joinTab;

    @FindBy(xpath = "//ul[@id='search-results']")
    List<WebElement> listOfSearchResult;

    @FindBy(xpath = "//span[text()='SHOP WOMEN']")
    private WebElement shopWomenButton;


    public HomePage(WebDriver driver) {
        super(driver);
    }


    public void openHomePage(String url) {
        driver.get(url);
    }

    public boolean visibilityOfLogo() {
        return logo.isDisplayed();
    }

    public void clickWomenButton() {
        womenButton.click();
    }

    public void clickMenButton() {
        menButton.click();
    }

    public void visibilityOfSearchField() {
        searchField.isDisplayed();
    }

    public void clickMyAccountDropdown() {
        myAccountDropdown.click();
    }

    public WebElement getSignInPopupElement(){
        return signInPopup;

    }

    public boolean getSignInPopup() {
        return signInPopup.isDisplayed();
    }

    public void clickSavedItemsButton() {
        savedItemsButton.click();
    }

    public void visibilityBagButton() {
        bagButton.isDisplayed();
    }

    public void clickBagButton() {
        bagButton.click();
    }

    public void visibilityFooter() {
        footer.isDisplayed();
    }


    public void clickCountrySelectorButton() {
        countrySelectorButton.click();
    }

    public boolean getSelectCountryPopup() {
        return countrySelectorForm.isDisplayed();
    }

    public void clickIdCountry() {
        idCountry.click();
    }

    public void selectCountryName() {
        countryName.isSelected();
    }

    public String getNameCountry() {
        return countryName.getText();
    }

    public void clickSubmitButton() {
        submitButton.submit();
    }

    public void clickJoinTab() {
        joinTab.click();
    }

    public void enterNameOfGoodInSearchField(final String searchName) {
        searchField.sendKeys(searchName);
    }


    public boolean checkThatSearchResultsContainsKeyword() {
        for (WebElement webElement : listOfSearchResult) {
            if (!webElement.getText().contains("Denim")) {
                return false;
            }
        }
        return true;
    }

    public void clickShopWomenButton() {
        shopWomenButton.click();
    }

}
