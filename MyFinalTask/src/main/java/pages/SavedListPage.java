package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SavedListPage extends BasePage {

    @FindBy(xpath = "//div[@class='loadedItemsWrapper_3QrER']")
    private WebElement chosenItems;

    @FindBy(xpath = "//div[@class='loadedItemsWrapper_3QrER']")
    List<WebElement> listOfChosenItems;

    @FindBy(xpath = "//div[contains(@class,'itemCount')][text()='2 items']")
    private WebElement itemCount;

    @FindBy(xpath = "//img[@class='productImage_2uJHI']")
    private List<WebElement> product;

    @FindBy(xpath = "//select[@data-id='sizeSelect']")
    private WebElement selectSizeField;

    @FindBy(xpath = "//option[contains(text(),'W28 L30')]")
    private WebElement selectSize;

    @FindBy(xpath = "//span[text()='Add to bag']")
    private WebElement addToBagButton;

    @FindBy(xpath = "//span[@class='_1z5n7CN']")
    private WebElement cart;

    @FindBy(xpath = "//span[@class='_1z5n7CN']")
    private WebElement cartPopup;

    @FindBy(xpath = "//span[@class='error basic-error-box']")
    private WebElement textError;


    public SavedListPage(WebDriver driver) {
        super(driver);
    }


    public WebElement getChosenItems() {
        return chosenItems;
    }

    public boolean visibilityChosenItems() {
        return chosenItems.isDisplayed();
    }

    public String numberOfItems() {
        return itemCount.getText();
    }

    public void clickProductSelect() {
        product.get(1).click();
    }

    public void clickProductAnotherSelect() {
        product.get(2).click();
    }

    public void clickSelectSizeField() {
        selectSizeField.click();
    }

    public void clickSelectSize() {
        selectSize.click();
    }

    public void clickAddToBAgButton() {
        addToBagButton.click();
    }

    public String getCartButtonText() {
        return cart.getText();
    }

    public WebElement getCartPopup() {
        return cartPopup;
    }

    public boolean visibilityTextError() {
        return textError.isDisplayed();
    }

    public String getTextError() {
        return textError.getText();
    }

}
