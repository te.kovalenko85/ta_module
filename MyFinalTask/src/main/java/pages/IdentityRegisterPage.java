package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IdentityRegisterPage extends BasePage {

    @FindBy(xpath = "//div[@id='content']")
    private WebElement registerOtherForm;


    public IdentityRegisterPage(WebDriver driver) {
        super(driver);
    }


    public boolean visibilityRegisterOtherForm() {
        return registerOtherForm.isDisplayed();
    }
}
