Feature: Main_elements
  As a user
  I want to test all main site functional
  So that I can be sure that site works correctly

  Scenario Outline: Check the presence of main elements
    Given User opens '<homePage>' page
    And User sees logo
    When User clicks women button
    Then User can see the page of women's products
    When User clicks men button
    Then User can see the page of men's products
    And User checks the searchField
    And User clicks account button
    And User can see popup to SignIn or Join
    And User clicks savedItemsButton
    And User checks the bagButton
    When User clicks bag button
    Then User can see bag page and SignIn button
    And User can see footer

    Examples:
      | homePage              |
      | https://www.asos.com/ |

  Scenario Outline: Check country selector
    Given User opens '<homePage>' page
    And User clicks country selector button
    And User can see popup of preferences
    And User choose country
    And User checks that country selector '<country>'

    Examples:
      | homePage              | country                      |
      | https://www.asos.com/ | https://www.asos.com/?r=1 |

  Scenario Outline: Check the SignUp
    Given User opens '<homePage>' page
    And User clicks MyAccount button
    And User clicks Join tab
    And User fills the register form with '<email address>', '<first name>', '<last name>', '<password>'
    When User clicks Join
    Then User can see the offer to finish registration

    Examples:
      | homePage              | email address         | first name | last name | password   |
      | https://www.asos.com/ | tetyanka_2007@ukr.net | Tetyana    | Tan       | Qw1234567! |


  Scenario Outline: Check the warning message with empty field
    Given User opens '<homePage>' page
    And User clicks MyAccount button
    And User clicks Join tab
    When User fills the register form with '<email address>', '<last name>', '<password>'
    Then User can see a warning message

    Examples:
      | homePage              | email address         | last name | password   |
      | https://www.asos.com/ | tetyanka_2007@ukr.net | Tan       | Qw1234567! |


  Scenario Outline: Check the warning message with invalid date of birth
    Given User opens '<homePage>' page
    And User clicks MyAccount button
    And User clicks Join tab
    And User fills the register form in fields '<email address>', '<first name>', '<last name>', '<password>'
    When User chooses nonexistent day of month
    Then User sees message about wrong date '<wrongMessage>'

    Examples:
      | homePage              | email address         | first name | last name | password   | wrongMessage                                                                    |
      | https://www.asos.com/ | tetyanka_2007@ukr.net | Tetyana    | Tan       | Qw1234567! | Enter your full date of birth |


  Scenario Outline: Check if the list of month days is sorted
    Given User opens '<homePage>' page
    And User clicks MyAccount button
    And User clicks Join tab
    And User clicks birthday box
    And User can see days are sorted in ascending


    Examples:
      | homePage              |
      | https://www.asos.com/ |


  Scenario Outline: Check the search function by keyword
    Given User opens '<homePage>' page
    And User enters '<keyword>' to find the good
    And User can see the list of goods by keyword

    Examples:
      | homePage              | keyword |
      | https://www.asos.com/ | denim   |


  Scenario Outline: Check the selection function of goods
    Given User opens '<homePage>' page
    And User clicks shop women button
    And User chooses category of goods HOURGLASS JEANS
    Then User chooses the items
    And User clicks saved items button
    And User can see chosen goods
    And User checks correct number of items '<quantity>'

    Examples:
      | homePage              | quantity |
      | https://www.asos.com/ | 2 items  |


  Scenario Outline: Check the adding function to cart
    Given User opens '<homePage>' page
    And User clicks shop women button
    And User chooses category of goods HOURGLASS JEANS
    Then User chooses the items
    And User clicks saved items button
    And User can see chosen goods
    And User clicks on the products
    Then User clicks the button to add goods
    And User checks quantity '<number>' of goods in the cart

    Examples:
      | homePage              | number |
      | https://www.asos.com/ | 1     |



  Scenario Outline: Check add to cart in case the size field is empty
    Given User opens '<homePage>' page
    And User clicks shop women button
    And User chooses category of goods HOURGLASS JEANS
    Then User chooses the items
    And User clicks saved items button
    And User can see chosen goods
    And User doesn't click on the select size field
    Then User clicks the button to add goods
    And User can see the message '<warningMessage>'

    Examples:
      | homePage              | warningMessage |
      | https://www.asos.com/ | Please select from the available colour and size options |



